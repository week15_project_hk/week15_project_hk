## Purpose
Anti-Theft and Security Survelliance

* The purpose of this project is to make a Raspberry Pi motion activated camera and make it data efficient so that less disk space is taken up. The camera will only be triggered if there is motion in the area and the video will be observed on the dashboard using a web browser. 
We also gather knowledge from IoT (Internet of things) such as Web Services/Web Sockets, IOT protocols (MQTT/CoAP and other protocols), energy awareness of IoT protocols, HTTP and how to do research.

## Equipment

* 1x - Raspberry Pi 3 Model B+
* 1x - Raspberry Pi Pinoir Camera V2 Camera MOD (daylight/night time) 
* 1x - Raspberry Pi - power supply
* 1x - SD card (max. 8 gb)
* 1x - Micro USB cable

## References
* https://www.hackster.io/daniel-jablonski/lane-tech-hs-pcl-raspberry-pi-motion-activated-camera-6de824?fbclid=IwAR2dHJ479L1gr4S_QXo8PKNCYKLmyDNL3BT7iv2yaJ18-nPlRNXJJqRFjZo
